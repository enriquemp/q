#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Debian Lab - Máquinas virtuales con Debian levantadas rápidamente

import os
import requests
import shutil

dirVms=os.path.expanduser('~')+"/vms"
dirVmsTemp=dirVms+"/templates"
dirVmsImgs=dirVms+"/images"
cloudImgs=["https://cloud.debian.org/images/cloud/buster/latest/debian-10-nocloud-amd64.qcow2", "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-nocloud-amd64.qcow2", "https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-amd64.qcow2"]
vmMemory=4096 # En Megas

def main():
  createDirs() # Comprobamos si existen los directorios y si no es así se crean.
  downloadTemplates() # Comprobamos si ya están bajadas las imagenes cloud y si no es así se bajan.
  choice() # Elegir entre Crear una nueva máquina virtual, ejecutar una ya existente o borrarla.

def createDirs():
  if not os.path.exists(dirVms):
    os.makedirs(dirVms)
    os.makedirs(dirVmsTemp)
    os.makedirs(dirVmsImgs)

def downloadTemplates():
  qcowTemplates = os.listdir(dirVmsTemp)
  if len(qcowTemplates) == 0:
   for i in range(len(cloudImgs)):
     localFilename = cloudImgs[i].split('/')[-1]
     session = requests.Session()
     response = session.get(cloudImgs[i],allow_redirects=True,stream=True)
     if response.status_code == 200:
      print("[+] Downloading \t"+cloudImgs[i]+"\t[200]")
      downloadQcow = session.get(cloudImgs[i], allow_redirects=True)
      open(dirVmsTemp+"/"+localFilename, 'wb').write(downloadQcow.content)

def choice():
  print("[1] New VM\n[2] Run VM\n[3] Del VM\n[4] Exit")
  option=input("\nOption: ")
  if option == "1": createVms()
  if option == "2": runVms()
  if option == "3": delVms()
  if option == "4": return 0


def createVms():
  qcowFiles = os.listdir(dirVmsTemp)
  qcowFiles.sort()
  vmName=input("\nVM Name: ")
  print("\nSeleccione una versión de Debian\n")
  for i in range(len(qcowFiles)):
    print("["+str(i)+"] - Debian "+qcowFiles[i].split('-')[-3]+" ("+qcowFiles[i].split('-')[-1]+")")
  vmVersion=input("\nVersion: ")
  shutil.copyfile(dirVmsTemp+"/"+qcowFiles[int(vmVersion)], dirVmsImgs+"/"+vmName+".qcow2")
  print("\nTamaño del disco en Gigas")
  vmSize=input("\nSize: ")
  os.system("qemu-img resize "+dirVmsImgs+"/"+vmName+".qcow2 "+vmSize+"G")
  print("\nPassword de root")
  vmPasswd=input("\nPassword: ")
  os.system("virt-customize -a "+dirVmsImgs+"/"+vmName+".qcow2 --root-password password:"+vmPasswd)
  os.system("virt-customize -a "+dirVmsImgs+"/"+vmName+".qcow2 --hostname "+vmName)
  os.system("virt-customize -a "+dirVmsImgs+"/"+vmName+".qcow2 --install dropbear")
  os.system("virt-customize -a "+dirVmsImgs+"/"+vmName+".qcow2 --firstboot-command 'growpart /dev/sda 1'")
  os.system("virt-customize -a "+dirVmsImgs+"/"+vmName+".qcow2 --firstboot-command 'resize2fs /dev/sda1'")
  choice()

def runVms():
  qcowFiles = os.listdir(dirVmsImgs)
  qcowFiles.sort()
  if len(qcowFiles) != 0:
    print("\nSeleccione una máquina para ejecutarla\n")
    for i in range(len(qcowFiles)):
      print("["+str(i)+"] - "+qcowFiles[i])
    vmRun=input("\nMachine: ")
    os.system("qemu-system-x86_64 -machine type=q35,accel=kvm -enable-kvm -device virtio-net-pci,netdev=net0 -netdev user,id=net0,hostfwd=tcp::2222-:22,hostfwd=tcp::8080-:8080 -smp $(nproc) --boot c -nographic -monitor telnet::45454,server,nowait -serial mon:stdio -m "+str(vmMemory)+" -hda "+dirVmsImgs+"/"+qcowFiles[int(vmRun)])
  choice()

def delVms():
  qcowFiles = os.listdir(dirVmsImgs)
  qcowFiles.sort()
  if len(qcowFiles) != 0:
    print("\nSeleccione una máquina para borrarla\n")
    for i in range(len(qcowFiles)):
      print("["+str(i)+"] - "+qcowFiles[i])
    vmDel=input("\nMachine: ")
    os.system("rm -rf "+dirVmsImgs+"/"+qcowFiles[int(vmDel)])
  choice()

main()
