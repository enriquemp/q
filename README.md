# q - lab of virtual machines with qemu

"q" is a script to quickly setup a lab of virtual machines to test things. 

**Installation:**

<pre>
$ git clone http://git.56k.es/fanta/q ; cd q
$ chmod +x q.py
# cp -pRv q.py /bin/q
</pre>

**Dependencies:**

- qemu-system-x86_64
- libguestfs-tools
